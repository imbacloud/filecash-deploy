# FileCash 一键部署脚本

## 1.单机部署(主要用于单机测试环境)
运行以下命令一键部署 Filecash，支持 Ubuntu 18.04/20.04 ,请使用root用户运行
~~~bash
wget https://gitee.com/imbacloud/filecash-deploy/raw/master/install.sh
bash install.sh
~~~

默认将注册16扇区矿工，如需更改，请修改脚本。

注意：脚本中的环境变量。

NVME固态硬盘缓存挂载路径: /ssd/

机械硬盘以及共享储存挂载路径: /hdd/

swap 120G 占用系统盘。

参数文件 90G，占用 /ssd/

欢迎进群讨论:[终于找到组织了] 
<div align="center">
<img src="img/mmexport1611327037904.jpg" height="400" alt="wechat">
<img src="img/qrcode_1611327021598.jpg" height="400" alt="qq">
</div>


**简单升级(通过替换执行文件升级)**

~~~bash
wget https://snapshot.file.cash/filecash-v1.5.0-amd-20.04.tar.gz -O filecash-v1.5.0-amd-20.04.tar.gz
./upgrade-simple.sh filecash-v1.5.0-amd-20.04.tar.gz

~~~
## 2.单机部署(同一节点，增加第二个矿工)
~~~bash
echo "export LOTUS_STORAGE_PATH=/ssd/lotusminer-16g" > env.sh
source env.sh
wget https://gitee.com/imbacloud/filecash-deploy/raw/master/service/lotus-miner-16g.service -O /lib/systemd/system/lotus-miner-16g.service
owner=$(lotus wallet default)
lotus wallet balance $owner
lotus-miner init --owner=$owner --sector-size=16GiB
sed -i "s/\"CanStore\": true/\"CanStore\": false/" ${LOTUS_STORAGE_PATH}/sectorstore.json
sed -i 's@#  ListenAddress = "/ip4/127.0.0.1/tcp/2345/http"@ListenAddress = "/ip4/127.0.0.1/tcp/2347/http"@' ${LOTUS_STORAGE_PATH}/config.toml
sed -i 's@#  RemoteListenAddress = "127.0.0.1:2345"@RemoteListenAddress = "127.0.0.1:2347"@' ${LOTUS_STORAGE_PATH}/config.toml
systemctl enable lotus-miner-16g
systemctl start lotus-miner-16g
sleep 65
lotus-miner storage attach --init  --store  /hdd/Store04/
~~~

## 3.集群安装 (主要用于生产环境)
### 3.1 集群架构
![lotus](img/lotus.jpg)

### 3.2 准备工作
~~~bash
sudo su - # 切换root账号
git clone https://gitee.com/imbacloud/filecash-deploy.git
cd filecash-deploy/cluster/
vim hosts  # 添加每一个主机的 hostname 和对应的ip 会替换所有主机的hosts
vim ansble-host # ansble 主机分组文件， 按实际主机数量分组

apt update
apt install -y ansible
cp -a hosts /etc/hosts
cp -a ansble-host /etc/ansible/hosts
# 制作无密登陆各个主机
ssh-keygen
for host in $(cat hosts | grep imba | awk '{print $2}')
do
	ssh-copy-id ${host}
done
# 此时已经可以通过ansble控制所有机器
ansible all -m shell -a 'hostname'

# 设置主机名、换成阿里源、替换所有主机hosts
ansible-playbook name.yml
~~~

### 3.3 参数设置
// TODO

### 3.4 环境安装
3.4.1 存储集群安装
~~~bash
ansible-playbook ceph-install.yml
~~~

### 3.5 一键安装
// TODO

### 3.6 监控安装
// TODO

### 3.7 运维工具
// TODO

# $捐$赠$

![donation](img/alipay.jpg)
