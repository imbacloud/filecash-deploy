#!/bin/bash
set -x
package=$1
if [ -z $package ]
then
	echo "no package selected"
	exit 0
else
	echo "upgrade package selected $package"
	systemctl stop lotus-miner.service
	systemctl stop lotus.service
	rm -f /usr/local/bin/lotus
	rm -f /usr/local/bin/lotus-miner
	rm -f /usr/local/bin/lotus-worker
	tar -zxvf $package -C /usr/local/bin/
	systemctl start lotus.service
	sleep 10
	systemctl status lotus.service
	lotus sync wait
	systemctl start lotus-miner.service
	sleep 10
	systemctl status lotus-miner.service
	echo "upgrade complete"
fi
