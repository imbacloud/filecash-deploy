#!/bin/bash
#set -x
function getpower(){
	for m in $(lotus state list-miners)
	do
		pow=`lotus state power $m | grep -v "0 B"`

	if [  -n "$pow" ]
	then
		p=`lotus state power $m`
		p2=`echo $p|awk '{match($0,/\([^()]*\)/);print substr($0,RSTART+1,RLENGTH-2)}'`
		p1=`echo $p|awk '{match($0,/[0-9]+/);print substr($0,RSTART,RLENGTH)}'`
		printf "%s %6s %9s %0.2f %2s\n" $p1 "$m" "$p2"
	fi
done
}

getpower | sort -t ' ' -k 1 -nr > /tmp/pow
c=0
cat /tmp/pow | while read line
do
	s1=`echo $line | awk '{print $2}'`
	s2=`echo $line | awk '{print $3}'`
	s3=`echo $line | awk '{print $4}'| head -c 1`
	c=$((${c} + 1))
	l=`cat map|grep $s1|awk '{print $2}'`
	f=`lotus-miner -a $s1 info|head -6 | grep Proving|awk '{match($0,/\([^()]*\)/);print substr($0,RSTART+1,RLENGTH-2)}'`
	if [ -n "$f" ]
	then
		f=" (${f})"
	else
		f=""
	fi
	w=`lotus wallet balance $s1|awk '{print $1}'`
	printf "%2s %6s %5s%s %9.2f%s %2s\n" "$c" "$s1" "$s2" "$s3" "$w" "$f" "$l"
done
