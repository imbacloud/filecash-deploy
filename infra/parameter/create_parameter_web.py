import ipfshttpclient
import json


def loadjson(jsonfile):
    with open(jsonfile) as json_file:
        data = json.load(json_file)
        return data


parameters = loadjson('parameters.json')
api = ipfshttpclient.connect('/ip4/127.0.0.1/tcp/5001', timeout=3600)

Nhash = api.object.new()
Nhash = Nhash['Hash']
print(Nhash)
for parameter in parameters.keys():
    Nhash = api.object.patch.add_link(Nhash, parameter, parameters[parameter]['cid'])
    Nhash = Nhash['Hash']
    print(Nhash)
print('http://127.0.0.1:8080/ipfs/%s' % Nhash)