# 使用说明

1. 将parameters.json放置与当前目录下

2. 修改脚本中ipfsapi的地址，parameters.json中的文件已经检索(pin固定)到当前ipfs节点

3. 安装依赖
~~~bash
# 可以将源换成阿里源
apt install python3-pip
# 可以将源换成阿里源
pip3 install -r requirements.txt

~~~

4. 运行
~~~bash
python3 create_parameter_web.py
~~~