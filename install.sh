#!/bin/bash
set -x
apt update

apt install -y jq mesa-opencl-icd ocl-icd-opencl-dev ntpdate ubuntu-drivers-common

ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
ntpdate ntp.aliyun.com


ulimit -n 1048576
sed -i "/nofile/d" /etc/security/limits.conf
echo "* hard nofile 1048576" >> /etc/security/limits.conf
echo "* soft nofile 1048576" >> /etc/security/limits.conf
echo "root hard nofile 1048576" >> /etc/security/limits.conf
echo "root soft nofile 1048576" >> /etc/security/limits.conf

if [ ! -d /ssd  ];then
  mkdir /ssd
fi
if [ ! -d /hdd  ];then
  mkdir /hdd
fi



SWAPSIZE=`swapon --show | awk 'NR==2 {print $3}'`
if [ "$SWAPSIZE" != "120G" ]; then
	OLDSWAPFILE=`swapon --show | awk 'NR==2 {print $1}'`
	NEWSWAPFILE="/swapfile"
	if [ -n "$OLDSWAPFILE" ]; then
		swapoff -v $OLDSWAPFILE
		rm $OLDSWAPFILE
		sed -i "/\\$OLDSWAPFILE/d" /etc/fstab
		NEWSWAPFILE=$OLDSWAPFILE
	fi
	fallocate -l 120GiB $NEWSWAPFILE
	chmod 600 $NEWSWAPFILE
	mkswap $NEWSWAPFILE
	swapon $NEWSWAPFILE
	echo "$NEWSWAPFILE none swap sw 0 0" >> /etc/fstab
	sysctl vm.swappiness=1
	sed -i "/swappiness/d" /etc/sysctl.conf
	echo "vm.swappiness=1" >> /etc/sysctl.conf
fi


function setenv(){
	sed -i "/$1/d" /etc/profile
	echo "export $1=$2" >> /etc/profile
}
setenv LOTUS_PATH /ssd/lotus
setenv LOTUS_STORAGE_PATH /ssd/lotusminer
setenv LOTUS_WORKER_PATH /ssd/lotusworker
setenv FIL_PROOFS_PARAMETER_CACHE /ssd/filecoin-proof-parameters
setenv FIL_PROOFS_SDR_PARENTS_CACHE_SIZE 1073741824
setenv RUST_BACKTRACE full
setenv RUST_LOG debug

source /etc/profile


wget https://snapshot.file.cash/filecash-v1.5.0-intel-18.04.tar.gz -O filecash-v1.5.0-intel-18.04.tar.gz
tar -zxvf filecash-v1.5.0-intel-18.04.tar.gz -C /usr/local/bin/

#wget https://snapshot.file.cash/filecash-v1.5.0-amd-20.04.tar.gz -O filecash-v1.5.0-amd-20.04.tar.gz
#tar -zxvf filecash-v1.5.0-amd-20.04.tar.gz -C /usr/local/bin/


wget https://gitee.com/imbacloud/filecash-deploy/raw/master/service/lotus.service -O /lib/systemd/system/lotus.service
wget https://gitee.com/imbacloud/filecash-deploy/raw/master/service/lotus-miner.service -O /lib/systemd/system/lotus-miner.service
systemctl daemon-reload




systemctl enable lotus
systemctl start lotus
sleep 65
lotus sync wait

owner=`lotus wallet new bls`

echo $owner

lotus wallet balance $owner

read -s -n1 -p "please send some FIC and press any key to continue ... "

lotus wallet balance $owner
lotus wallet balance $owner

lotus-miner init --owner=$owner --sector-size=16GiB

sed -i "s/\"CanStore\": true/\"CanStore\": false/" ${LOTUS_STORAGE_PATH}/sectorstore.json

systemctl enable lotus-miner
systemctl start lotus-miner
sleep 65
lotus-miner storage attach --init  --store  /hdd/Store03/